-- Inf2D Assignment 1 (Last updated: 25 Jan 2016)

-- Good Scholarly Practice
-- Please remember the University requirement as regards all assessed work for credit. 
-- Details about this can be found at:
-- http://www.ed.ac.uk/academic-services/students/undergraduate/discipline/academic-misconduct
-- and at:
-- http://web.inf.ed.ac.uk/infweb/admin/policies/academic-misconduct
-- Furthermore, you are required to take reasonable measures to protect your assessed work from 
-- unauthorised access.For example, if you put any such work on a public repository then you must 
-- set access permissions appropriately (generally permitting access only to yourself, or your 
-- group in the case of group practicals).

module Inf2d where
import Data.List
import Debug.Trace
import Data.Ord
import Data.Maybe
import Data.Char  -- I've imported this for the isAlpha function
import Data.Set  -- I've imported this for the set function
import Data.Time




-- Type synonyms for the data structures
-- Symbols are strings (a negative sign as the first character represents a negated symbol)
type Symbol = String  
-- Clause = a disjuntion of symbols
type Clause = [Symbol] 
-- Sentence = Statements. This is a list of a list of symbols
type Sentence = [[Symbol]] 
-- Models are represented as a list of (Symbol,Boolean) tuples
type Model = [(Symbol, Bool)]
-- The knowledge base is represented as a list of statements
type KB = [Sentence]


-----------------------------------------------
-- STUDENT MATRICULATION NUMBER: 
-----------------------------------------------
studentId::String
studentId = "s1453370"

--------------------------------------------------
-- ASSIGNMENT TASKS
-- Refer to assignment sheet for details of tasks
--------------------------------------------------


-- Please read the comments for clarification on what I made certain functions do, based on
-- the assignment, the given comments here and the answers on Piazza.


----------TASK 1: REPRESENTATION (2 marks)----------------------------------------------------------
wumpusFact::Sentence
wumpusFact = [["-B11","P11","P22","P31"],["-P11","B11"],["P22","B11"],["P31","B11"]]

----------TASK 2: GENERAL HELPER FUNCTIONS (10 marks)-----------------------------------------------

-- Finds the assigned literal to a symbol from a given model
-- I assume that this function doesn't look up negated symbols, i.e. if we look lookup a "-P" in [("P",True)], we return Nothing.
lookupAssignment :: Symbol -> Model -> Maybe Bool
lookupAssignment  symbol [] = Nothing
lookupAssignment symbol (firstTuple:restModel)
                                    | symbol == fst(firstTuple) = Just (snd(firstTuple))
                                    | otherwise = lookupAssignment symbol restModel

-- Negate a symbol
negateSymbol :: Symbol -> Symbol
negateSymbol "" = ""
negateSymbol symbol 
                    | isAlpha(head(symbol)) = "-" ++ symbol --if sym is not negated
                    | otherwise = tail(symbol) --else if it is negated

-- For a given symbol, this function checks if it is negated(i.e., has a negation sign).
isNegated :: Symbol -> Bool
isNegated symbol 
                 | isAlpha(head(symbol)) = False
                 | (head(symbol)) == '-' = True
                 | otherwise = error "Invalid symbol"


-- This function takes a symbol and returns an Symbol without any negation sign if the original 
-- symbol had one.

getUnsignedSymbol :: Symbol -> Symbol
getUnsignedSymbol symbol 
                         | isAlpha(head(symbol)) = symbol  -- I assume here that all symbols are of valid format
                         | otherwise = tail(symbol)


-- Gets a list of all symbols in for all given sentences

-- Create one big sentence from the list of sentences - helper
bigSentenceHelper::[Sentence] -> Sentence
bigSentenceHelper [] = []
bigSentenceHelper (x:xs) = x++bigSentenceHelper xs

-- Create a list of symbols from a single sentence - helper
allSymbolsHelper :: Sentence -> [Symbol]
allSymbolsHelper [] = []
allSymbolsHelper (x:xs) = x++allSymbolsHelper xs


-- Extract all symbols from all sentences and put them in a list of symbols
allSyms :: [Sentence] -> [Symbol] 
allSyms xs = allSymbolsHelper $ bigSentenceHelper xs

-- Convert all symbols into their unsigned counterparts (I assume that's what is wanted)
allSymsUnsigned :: [Symbol] -> [Symbol]
allSymsUnsigned lst = Data.List.map getUnsignedSymbol lst

-- Use the set nub function imported from the Data.Set library eliminate duplicates
getSymbols :: [Sentence] -> [Symbol]
getSymbols stmts = nub $ allSymsUnsigned $ allSyms stmts

----------TASK 3: TRUTH TABLE ENUMERATION AND ENTAILMENT (40 marks)---------------------------------

-- Function takes as input a list of symbols, and returns a list of models (all possible assignment 
-- of True or False to the symbols.)

--Generate all combinations of Trues and Falses for a list of length n - helper.

--This helper function will start to create an infinite list of binary space and will return
--the binary space(all models) of n variables, specified as argument

generateBinarySpace n = (iterate (concatMap(\xs -> [True:xs, False:xs])) [[]])!!n

generateModels :: [Symbol] -> [Model]
generateModels [] = [] -- if no symbols, no models
-- generate the appropriate binary space, then for each element(which is a list of bools) in the binary space list,
-- take it as an argument to the zip function and apply it with the model.
generateModels x = let assignments = generateBinarySpace (length x) in Data.List.map (\y->zip x y) assignments

--IGNORE THE FOLLOWING JUNK COMMENTS

--bla = concatMap(\xs -> [True:xs, False:xs]) [[]]
-- this will be inferred as having the type [Bool] -> [[Bool]], since we have True:empt, so empt must be [Bool]
--xs empt = [True:empt, False:empt]   
--sdaf = concatMap(\xs -> xs:[1,2,3,4])[1,2,3,4]
-- this will be inferred as having the type [[[Bool]]], since it doesn't have an argument but it is interpreted that
-- xs must be [Bool]
--df = Data.List.map (\xs -> [True:xs, False:xs]) [[]]
-- df concatenates True to the empty list(takes the empty list as an argument)
--generateModels (sym:symbols) = [(sym,True)] ++ generateModels(symbols) ++ [(sym,False)] ++ generateModels(symbols)

--JUNK COMMENTS OVER


-- This function evaluates the truth value of a propositional sentence using the symbols 
-- assignments in the model.


--Helper - evaluate truth of a single clause ([Symbol]=Clause)
--I assume here that every symbol in the clause must have an assignment in the model, later version of that function doesn't have that assumption.
--A clause can be evaluated if it's empty, all all of the syms in the clause or false, or there's at least one true.
evaluateClauseTruth :: [Symbol] -> Model ->  Bool
evaluateClauseTruth symbols model  
                                      | length(symbols)==0 = False --consider case where we're given the empty clause - empty clause is False
                                      | length(symbols)>0 && falsesInClause symbols model == length(symbols) =  False -- clause is false
-- if every symbol is false
                                      | length(symbols)>0 && truesInClause symbols model >=1 = True -- true if there's at least one true symbol
                                      | otherwise = error "Not enough symbols in the model"



-- check if symbol in clause is false depending on negation and model
conditionalFalse:: Symbol -> Model -> Bool
conditionalFalse sym model 
                       | (lookupAssignment (getUnsignedSymbol sym) model==Just True && isNegated(sym)==True) || lookupAssignment(getUnsignedSymbol sym) model== Just False && not(isNegated(sym)) = True
                       | otherwise = False -- if it's true or nothing return false

--count number of falses in a clause                                     
falsesInClause :: [Symbol] -> Model -> Int
falsesInClause syms model = sum[1| sym<-syms ,conditionalFalse sym model]

-- check if symbol in clause is true depending on negation and model
conditionalTrue sym model 
                           | (lookupAssignment (getUnsignedSymbol sym) model==Just True && isNegated(sym)==False) || lookupAssignment(getUnsignedSymbol sym) model== Just False && (isNegated(sym)) = True
                           | otherwise = False

--count number of trues in a clause
truesInClause syms model = sum[1| sym<-syms ,conditionalTrue sym model]


--Use the evaluateClauseTruth function to evaluate the truth of the sentence recursively
pLogicEvaluate :: Sentence -> Model -> Bool
pLogicEvaluate [] model = True -- empty conjunct clause is true
pLogicEvaluate (clause:clauses) model
                                      | evaluateClauseTruth clause model == True = True &&(pLogicEvaluate clauses model)
                                      | evaluateClauseTruth clause model == False = False

                                      | evaluateClauseTruth clause model == error "Not enough symbols in model" = error "Not enough symbols in model"
                                      
-- This function checks the truth value of list of a propositional sentence using the symbols 
-- assignments in the model. It returns true only when all sentences in the list are true.

plTrue :: [Sentence]-> Model -> Bool
plTrue [] model = True
plTrue sentences model = and[pLogicEvaluate sentence model|sentence<-sentences]


-- helper - find all models that satisfy a KB
-- Generate all models, then from those extract those that satisfy the KB.
helperFindKnowledgeModels :: [Sentence] -> [Symbol] -> [Model]
helperFindKnowledgeModels sents syms = [model|model<-modelGenerator, plTrue sents model==True] where modelGenerator = generateModels syms

-- This function takes as input a knowledgebase (i.e. a list of propositional sentences), 
-- a query (i.e. a propositional sentence), and a list of symbols.
-- IT recursively enumerates the models of the domain using its symbols to check if there 
-- is a model that satisfies the knowledge base and the query. It returns a list of all such models.

--Based on the comment above, I assume that this function must return all models that satisfy both the query and the knowledgebase,
--even though this does not tells us whether KB |= query.
--First it finds all models that satisfy and knowledgebase and from those it filters those that satisfy the query.
--I also assume that the list of symbols here consists of the union of symbols in the KB and the query.

ttCheckAll :: [Sentence] -> Sentence -> [Symbol] -> [Model]
ttCheckAll kb query symbols = Data.List.filter (\x -> pLogicEvaluate query x) (helperFindKnowledgeModels kb symbols)

-- Helper - get the union of all symbols in the knowledge base and the query

unionSymsKnowQuery :: [Sentence] -> Sentence -> [Symbol]
unionSymsKnowQuery base query = getSymbols (base++[query])

--return length of the list of models returned by helperFindKnowledgeModels,i.e the length of the list of models 
--that satisfy the KB
helperKnowLen :: [Sentence] -> [Symbol] -> Int
helperKnowLen sents syms = length(helperFindKnowledgeModels sents syms)

--return length of the list of models returned by ttCheckAllLen,i.e return the length of the list of models 
--that satisfy both the KB and the query
ttCheckAllLen :: [Sentence] -> Sentence -> [Symbol] -> Int
ttCheckAllLen base query syms = length(ttCheckAll base query syms)

--This function determines if a model satisfes both the knowledge base and the query, returning
--true or false.

--It checks if the length of the list of models satisfying the KB equals the length
--of the list of models that satisfy both the KB and the query, i.e. the checkAll function must not
--filter any models that satisfy the KB - all of them must satisfy the query as well.
ttEntails :: [Sentence] -> Sentence -> Bool
ttEntails kb query 
                    | (helperKnowLen kb (unionSymsKnowQuery kb query)) == (ttCheckAllLen kb query (unionSymsKnowQuery kb query))  = True
                    | otherwise = False

-- This function determines if a model satisfes both the knowledge base and the query. 
-- It returns a list of all models for which the knowledge base entails the query.

-- My understanding is that this function returns the models for which KB |= query if KB |= query is True.
-- A KB |= a query if all the models that satisfy the KB also satisfy the query.
-- Therefore I've implemented the ttEntailsModels so that if KB |= query (using ttEntails to determine whether this is true or alse), then
-- use ttCheckAll function to return the list of those models. If KB does not |= query, then return the empty list.
-- In my implementation ttEntails does not return the same list as ttCheckAll, because ttCheckAll does not tell us anything
-- about entailment based on the returned list, whereas here we return the models if KB |= query or nothing if KB does not |= query.

ttEntailsModels :: [Sentence] -> Sentence -> [Model]
ttEntailsModels kb query = if ttEntails kb query == True then ttCheckAll kb query (unionSymsKnowQuery kb query) else  []


----------TASK 4: DPLL (43 marks)-------------------------------------------------------------------

-- The early termination function checks if a sentence is true or false even with a 
-- partially completed model.  - no,piazza says otherwise, please give accurate instructions

--Helper - checks if a clause can be computed
computableClause::Clause ->Model-> Bool
computableClause symbols model
                                      | length(symbols)==0 = True --consider case where we're given the empty clause - empty clause is False
                                      | length(symbols)>0 && falsesInClause symbols model == length(symbols) =  True -- clause is false
-- if every symbol is false
                                      | length(symbols)>0 && truesInClause symbols model >=1 = True -- true if there's at least one true symbol
                                      | otherwise = False

--count how many clauses are computable -- not used for anything yet
countComputables::Sentence ->Model-> Int
countComputables sent model = sum[1| clause<-sent ,computableClause clause model]

--count the number of true clauses
countTrueClauses:: Sentence -> Model -> Int
countTrueClauses sent mod = sum[1|clause<-sent, evaluateClauseTruthET clause mod==Just True ]

--A clause can be evaluated if it's empty, all all of the syms in the clause or false, or there's at least one true.
--Same as evaluateClauseTruth but partial model can be used - returns Just True or Just False correspondingly,
--or nothing if clause can't be evaluated
evaluateClauseTruthET :: [Symbol] -> Model ->Maybe Bool
evaluateClauseTruthET symbols model  
                                      | length(symbols)==0 =Just False --consider case where we're given the empty clause - empty clause is False
                                      | length(symbols)>0 && falsesInClause symbols model == length(symbols) =Just False -- clause is false
-- if every symbol is false
                                      | length(symbols)>0 && truesInClause symbols model >=1 =Just True -- true if there's at least one true symbol
                                      | otherwise = Nothing
                                      | model == [] = Nothing

countNothClauses:: Sentence -> Model -> Int
countNothClauses sent mod = sum[1|clause<-sent, evaluateClauseTruthET clause mod==Nothing ]


--count the number of false clauses
countFalseClauses:: Sentence -> Model -> Int
countFalseClauses sent mod = sum[1|clause<-sent, evaluateClauseTruthET clause mod==Just False]

--early terminate checks if a sentence can be computed with a partial model
earlyTerminate :: Sentence -> Model -> Bool
earlyTerminate sent model 
                                      | length(sent)==0 = True -- empty conjunct is true
                                      | length(sent)>=1 && countTrueClauses sent model == length(sent) = True
                                      | length(sent)>=1 && countFalseClauses sent model>=1 = True
                                      | otherwise = False

--the function is the same as pLogicEvaluate, but is of Maybe type, unlike pLogicEvaluate which needs a full model,
--this will return the value of a sentence with a partial model and Nothing if it can't be evaluated - used in DPLL
pLogicEvaluateET :: Sentence -> Model ->Maybe Bool
pLogicEvaluateET [] model =Just True -- empty conjunct clause is true
pLogicEvaluateET sent model
                                      |length(sent)==0= Just True
                                      |length(sent)>=1&&countTrueClauses sent model == length(sent)=Just True
                                      |length(sent)>=1 && countFalseClauses sent model == 0 = Nothing
                                      |length(sent)>=1&&countFalseClauses sent model >=1 = Just False

-- This function finds pure symbol, i.e, a symbol that always appears with the same "sign" in all 
-- clauses.
-- It takes a list of symbols, a list of clauses and a model as inputs. 
-- It returns Just a tuple of a symbol and the truth value to assign to that
-- symbol. If no pure symbol is found, it should return Nothing

-- helper - check if item is in list
itemInClause::Symbol -> Clause -> Clause
itemInClause sym [] = []
itemInClause sym (cl:clause) 
                           | sym == getUnsignedSymbol cl = [cl] ++(itemInClause sym clause)
                           | otherwise = itemInClause sym clause

itemInSentence::Symbol -> [Clause] -> Clause
itemInSentence sym [] = []
itemInSentence sym (cl:clause) = itemInClause sym cl ++ itemInSentence sym clause


--I took this function from stackoverflow - checks if all elements of a list are equal, i.e. I check here if symbols are of the same polarity
allTheSame:: Clause -> Bool
allTheSame xs = and $ Data.List.map (== head xs) (tail xs)


eliminateTrueClauses:: [Clause] -> Model ->[Clause]
eliminateTrueClauses cl [] = cl
eliminateTrueClauses clauses model = Data.List.filter(\x->evaluateClauseTruthET x model== Just True) clauses

--finds pure symbol, first eliminating clauses that are already true in the model
--and it will return it iff its corresponding unsigned is not already in the model
findPureSymbol :: [Symbol] -> [Clause] -> Model -> Maybe (Symbol, Bool)
findPureSymbol [] clauses model = Nothing
findPureSymbol (sym:symbols) clauses model 
                                            | (itemInSentence sym (eliminateTrueClauses clauses model)) == [] = findPureSymbol symbols clauses model
                                            | isNegated(head(itemInSentence sym (eliminateTrueClauses clauses model)))==True && allTheSame(itemInSentence sym (eliminateTrueClauses clauses model))==True && lookupAssignment(getUnsignedSymbol sym) model==Nothing = Just (sym,False)
                                            | not(isNegated(head(itemInSentence sym (eliminateTrueClauses clauses model))))==True && allTheSame(itemInSentence sym(eliminateTrueClauses clauses model))==True &&lookupAssignment(getUnsignedSymbol sym) model==Nothing = Just (sym,True)
                                            | not(allTheSame(itemInSentence sym (eliminateTrueClauses clauses model)))== True = findPureSymbol symbols clauses model
                                            |lookupAssignment(getUnsignedSymbol sym) model/=Nothing = findPureSymbol symbols clauses model


--check if a literal is false by consulting its unsigned corresponding value in the model
literalFalse::Symbol -> Model -> Bool
literalFalse sym model 
                       | (isNegated(sym)==True) && (lookupAssignment (getUnsignedSymbol sym) model==Just True) = True
                       | (isNegated(sym)==False) && (lookupAssignment (getUnsignedSymbol sym) model==Just False) = True
                       | (isNegated(sym)==False) && ((lookupAssignment sym model)==Just True)=False
                       | lookupAssignment sym model==Nothing=False
                       | otherwise = False


--filters all the literals in a clause, so that all that remain are not false
purifiedCl::Clause->Model->Clause
purifiedCl cl mod = Data.List.filter(\x->not(literalFalse x mod)) cl

--checks if after the filtering there's only one literal left and it has no value in the model
purifyClause :: Clause -> Model -> Bool
purifyClause clause model = if length(pureClause)==1&&lookupAssignment(head(pureClause))model == Nothing then  True else  False where 
   pureClause = purifiedCl clause model


-- This function finds a unit clause from a given list of clauses and a model of assignments.
-- It returns Just a tuple of a symbol and the truth value to assign to that symbol. If no unit  
-- clause is found, it should return Nothing. It will return 
-- a Just value iff a clause with only 1 literal is found
-- or if it's more than 1 literal, then the rest are assigned false
-- and the found literal must not be assigned a value in the model - lookup must be Nothing for that symbol.

--Recursively checks all clauses in a sentence to find a pure symbol
findUnitClause :: [Clause] -> Model -> Maybe (Symbol, Bool)
findUnitClause [] model = Nothing
findUnitClause (clause:clauses) model 
                                        | purifyClause clause model == True && not(isNegated(head(purifiedCl clause model)))= Just((head(purifiedCl clause model)),True)
                                        | purifyClause clause model == True && (isNegated(head(purifiedCl clause model)))= Just(getUnsignedSymbol(head(purifiedCl clause model)),False)
                                        | otherwise = findUnitClause clauses model


--name says it all - used in DPLL
fromMaybeToType:: Maybe(Symbol, Bool) -> (Symbol,Bool)
fromMaybeToType (Just y) = y

--assign symbol which doesn't occur in the model to a true - used in DPLL

assignSymToTrue::[Symbol] -> Model -> Model
assignSymToTrue [] model = []
assignSymToTrue (x:xs) model = if lookupAssignment (getUnsignedSymbol x) model == Nothing then model ++ [(getUnsignedSymbol x,True)]
                               else assignSymToTrue xs model

--assign symbol which doesn't occur in the model to a false - used in DPLL
assignSymToFalse::[Symbol] -> Model -> Model
assignSymToFalse [] model = []
assignSymToFalse (x:xs) model = if lookupAssignment (getUnsignedSymbol x) model == Nothing then model ++ [(getUnsignedSymbol x,True)]
                               else assignSymToTrue xs model

-- This function check the satisfability of a sentence in propositional logic. It takes as input a 
-- list of clauses in CNF, a list of symbols for the domain, and model. 
-- It returns true if there is a model which satises the propositional sentence. 
-- Otherwise it returns false.

--My implementation starts from an empty model, to which tuples of symbols and their values are added up
dpll :: [Clause] -> [Symbol] -> Bool
dpll clauses symbols = exhaustiveDPLL clauses symbols []


exhaustiveDPLL::[Clause] -> [Symbol] ->Model -> Bool
exhaustiveDPLL clauses sym model 
                                    |(earlyTerminate clauses model == True)&&(pLogicEvaluateET clauses model==Just True)=True
                                    |(earlyTerminate clauses model == True)&&(pLogicEvaluateET clauses model==Just False) = False
                                    |(earlyTerminate clauses model == False)&&(findUnitClause clauses model /= Nothing)=exhaustiveDPLL clauses sym (model++[fromMaybeToType(findUnitClause clauses model)])
                                    |(earlyTerminate clauses model == False)&&(findUnitClause clauses model == Nothing)&&(findPureSymbol sym clauses model /= Nothing) = exhaustiveDPLL clauses sym (model++[fromMaybeToType(findPureSymbol sym clauses model)])
                                    |(earlyTerminate clauses model == False)&&(findUnitClause clauses model == Nothing)&&(findPureSymbol sym clauses model == Nothing) = exhaustiveDPLL clauses sym (assignSymToTrue sym model) ||exhaustiveDPLL clauses sym (assignSymToFalse sym model)
                                    
                                  
              
-- This function serves as the entry point to the dpll function. It takes a list clauses in CNF as 
-- input and returns true or false. 
-- It uses the dpll function above to determine the satisability of its input sentence.
dpllSatisfiable :: [Clause] -> Bool
dpllSatisfiable clauses = dpll clauses (getSymbols [clauses])

----------TASK 5: EVALUATION (5 marks)--------------------------------------------------------------
-- EVALUATION
-- a knowledge base (i.e. a sentence in propositional 
-- logic), and a query sentence. Both items should have their clauses in CNF representation 
-- and should be assigned to the following respectively:
evalKB :: [Sentence]
evalKB = [ [["a","b"],["-b","c"]], [["-a"],["-b","c"]] ]

evalQuery :: Sentence
evalQuery = [["a","b"],["-b","c"]]


-- RUNTIMES
-- Enter the average runtimes of the ttEntails and dpllSatisable functions respectively

--here are the runtimes, as measured using the command ':set +s'
--ttEntails is run with inputs evalKB evalQuery,
--dpllSatisfiable is run with inputs evalKb and not query
--and all that converted from [sentence] to sentence
--comparing proof by resolution by enumeration of all models

runtimeTtEntails :: Double
runtimeTtEntails = 0.12 --seconds - time on the inputs above

runtimeDpll :: Double
runtimeDpll = 0.04 -- seconds on input [["a","b"],["-b","c"],["-a"],["-b","c"]] (the KB)



